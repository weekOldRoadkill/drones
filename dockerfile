FROM debian:bookworm
RUN apt-get update
RUN apt-get install -y pypy3 curl gcc
RUN curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y
RUN /root/.cargo/bin/rustup target add wasm32-unknown-unknown
COPY . /drones
WORKDIR /drones
RUN /root/.cargo/bin/cargo b --profile release-lto --target wasm32-unknown-unknown
RUN cp /drones/target/wasm32-unknown-unknown/release-lto/drones.wasm /drones/web
WORKDIR /drones/web
CMD pypy3 -m http.server