// Namespaces
use crate::util::*;

// State struct
pub(crate) struct State {
    pub(crate) fps: Fps,
    camera: camera::Camera2D,
    scale: f32,
    fullscreen: bool,
    drone: Drone,
    pub(crate) gravity: Vec2,
    pub(crate) drone_assets: DroneAssets,
}

// State implementation
impl State {
    // Update the state
    pub(crate) fn update(self: &mut Self) -> () {
        self.fps.tick();
        self.reset_drone();
        self.randomize_target();
        self.update_drone();
        self.toggle_fullscreen();
        self.update_camera();
    }

    // Render the state
    pub(crate) fn render(self: &mut Self) -> Result<()> {
        camera::set_camera(&self.camera);
        self.drone
            .render(&self)
            .wrap_err("Failed to render drone")?;
        camera::set_default_camera();
        self.render_ui();
        Ok(())
    }

    // Reset drone
    fn reset_drone(self: &mut Self) -> () {
        if input::is_key_pressed(RESET_DRONE_KEY) {
            self.drone.reset();
        }
    }

    // Toggle fullscreen
    fn toggle_fullscreen(self: &mut Self) -> () {
        if input::is_key_pressed(FULLSCREEN_KEY) {
            self.fullscreen = !self.fullscreen;
            window::set_fullscreen(self.fullscreen);
        }
    }

    // Update camera
    fn update_camera(self: &mut Self) -> () {
        let res = Vec2::new(window::screen_width(), window::screen_height());
        self.scale = res.min_element();
        self.camera = camera::Camera2D {
            zoom: self.scale / res,
            target: self.drone.pos,
            ..Default::default()
        };
    }

    // Render UI
    fn render_ui(self: &mut Self) -> () {
        let scale = (0.05 * self.scale).round();
        let label_style = ui::root_ui().style_builder().text_color(FOREGROUND).build();
        let title_label_style = ui::root_ui()
            .style_builder()
            .text_color(FOREGROUND)
            .font_size(scale as u16)
            .build();
        let window_style = ui::root_ui()
            .style_builder()
            .color(color::Color {
                a: 0.3,
                ..color::BLACK
            })
            .background_margin(math::RectOffset::new(
                0.1 * scale,
                0.1 * scale,
                0.05 * scale,
                0.05 * scale,
            ))
            .build();
        let editbox_style = ui::root_ui()
            .style_builder()
            .color(TRANSPARENT)
            .text_color(FOREGROUND)
            .build();
        let scrollbar_style = ui::root_ui().style_builder().color(TRANSPARENT).build();
        let scrollbar_handle_style = ui::root_ui()
            .style_builder()
            .color(FOREGROUND)
            .color_hovered(FOREGROUND)
            .color_clicked(FOREGROUND)
            .build();
        let checkbox_style = ui::root_ui()
            .style_builder()
            .color(FOREGROUND)
            .color_hovered(FOREGROUND)
            .color_clicked(FOREGROUND)
            .build();
        let skin = ui::Skin {
            label_style,
            window_style,
            editbox_style,
            scrollbar_style,
            scrollbar_handle_style,
            checkbox_style,
            scroll_multiplier: 0.1,
            ..ui::root_ui().default_skin()
        };
        let title_skin = ui::Skin {
            label_style: title_label_style,
            ..skin.clone()
        };
        ui::root_ui().push_skin(&title_skin);
        ui::root_ui().window(
            hash!(),
            Vec2::splat((0.25 * scale).round()),
            (scale * Vec2::new(8.0, 4.0)).round(),
            |ui| {
                ui.label(None, &format!("Fps: {}", self.fps.fps));
                let terms = self.drone.pos_pids.0.deref_mut();
                ui.label(None, "Horizontal PID:");
                ui.push_skin(&skin);
                ui.slider(hash!(), "Kp", -5.0..5.0, &mut terms.kp);
                ui.slider(hash!(), "Ki", -1.0..1.0, &mut terms.ki);
                ui.slider(hash!(), "Kd", -1.0..1.0, &mut terms.kd);
                ui.slider(hash!(), "Bias", -1.0..1.0, &mut terms.bias);
                ui.label(None, "");
                ui.pop_skin();
                let terms = self.drone.pos_pids.1.deref_mut();
                ui.label(None, "Vertical PID:");
                ui.push_skin(&skin);
                ui.slider(hash!(), "Kp", -5.0..5.0, &mut terms.kp);
                ui.slider(hash!(), "Ki", -1.0..1.0, &mut terms.ki);
                ui.slider(hash!(), "Kd", -1.0..1.0, &mut terms.kd);
                ui.slider(hash!(), "Bias", -1.0..1.0, &mut terms.bias);
                ui.label(None, "");
                ui.pop_skin();
                let terms = self.drone.ang_pid.deref_mut();
                ui.label(None, "Angular PID:");
                ui.push_skin(&skin);
                ui.slider(hash!(), "Kp", -5.0..5.0, &mut terms.kp);
                ui.slider(hash!(), "Ki", -1.0..1.0, &mut terms.ki);
                ui.slider(hash!(), "Kd", -1.0..1.0, &mut terms.kd);
                ui.slider(hash!(), "Bias", -1.0..1.0, &mut terms.bias);
                ui.label(None, "");
                ui.pop_skin();
            },
        );
        ui::root_ui().pop_skin();
    }

    // Randomize target
    fn randomize_target(self: &mut Self) -> () {
        if input::is_key_pressed(RANDOMIZE_TARGET_KEY) {
            self.drone.gen_target();
        }
    }

    // Update drone
    fn update_drone(self: &mut Self) -> () {
        let mut drone = mem::take(&mut self.drone);
        drone.update(&self);
        self.drone = drone;
    }
}

// Default state implementation
impl Default for State {
    // Default state
    fn default() -> Self {
        Self {
            fps: Default::default(),
            camera: Default::default(),
            scale: 0.0,
            fullscreen: FULLSCREEN,
            drone: Default::default(),
            gravity: Vec2::Y,
            drone_assets: Default::default(),
        }
    }
}
