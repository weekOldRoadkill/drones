// Namespaces
use crate::util::*;

// PID terms struct
#[derive(Clone)]
pub(crate) struct PIDTerms {
    pub(crate) kp: f32,
    pub(crate) ki: f32,
    pub(crate) kd: f32,
    pub(crate) bias: f32,
}

// Default PID terms implementation
impl Default for PIDTerms {
    // Default PID terms
    fn default() -> Self {
        Self {
            kp: 2.5,
            ki: 0.0,
            kd: 0.1,
            bias: 0.0,
        }
    }
}

// PID struct
pub(crate) struct PID {
    terms: Vec<(PIDTerms, f32)>,
    current: usize,
    timer: f32,
    error: f32,
    int: f32,
    deriv: f32,
}

// PID implementation
impl PID {
    // Contants
    const EVAL_PERIOD: f32 = 2.5;

    // Update the PID
    pub(crate) fn update(self: &mut Self, error: f32, delta: f32) -> f32 {
        self.int += delta * error;
        self.deriv = (error - self.error) / delta;
        self.error = error;
        if self.timer > Self::EVAL_PERIOD {
            self.terms[self.current].1 /= Self::EVAL_PERIOD;
            self.timer -= Self::EVAL_PERIOD;
            if let Some(min) = self
                .terms
                .iter()
                .enumerate()
                .min_by(|a, b| a.1 .1.partial_cmp(&b.1 .1).unwrap_or(cmp::Ordering::Equal))
            {
                if min.0 == self.current {
                    self.terms.push(((**self).clone(), 0.0));
                    self.current = self.terms.len() - 1;
                    let f = |x: &mut _| {
                        if rand::gen_range(0.0, 1.0) < 0.25 {
                            *x += rand::gen_range(-0.1, 0.1)
                        }
                    };
                    let terms = self.deref_mut();
                    f(&mut terms.kp);
                    f(&mut terms.ki);
                    f(&mut terms.kd);
                    f(&mut terms.bias);
                } else {
                    self.current = min.0;
                    self.terms[self.current].1 = 0.0;
                }
            } else {
                self.terms[self.current].1 += delta * self.error.abs();
            }
        }
        self.timer += delta;
        (**self).kp * self.error + (**self).ki * self.int + (**self).kd * self.deriv + (**self).bias
    }
}

// PID deref implementation
impl Deref for PID {
    // Deref target
    type Target = PIDTerms;

    // Deref
    fn deref(self: &Self) -> &Self::Target {
        &self.terms[self.current].0
    }
}

// PID mutable deref implementation
impl DerefMut for PID {
    // Mutable deref
    fn deref_mut(self: &mut Self) -> &mut Self::Target {
        &mut self.terms[self.current].0
    }
}

// Default PID implementation
impl Default for PID {
    // Default PID
    fn default() -> Self {
        Self {
            terms: vec![(Default::default(), 0.0)],
            current: 0,
            timer: 0.0,
            error: 0.0,
            int: 0.0,
            deriv: 0.0,
        }
    }
}
