// Modules
mod drone;
mod pid;
mod state;
mod util;

// Namespaces
use crate::util::*;

// Window configuration
fn window_conf() -> minq::conf::Conf {
    minq::conf::Conf {
        window_title: TITLE.into(),
        high_dpi: true,
        fullscreen: FULLSCREEN,
        sample_count: 8,
        icon: None,
        platform: minq::conf::Platform {
            webgl_version: minq::conf::WebGLVersion::WebGL2,
            swap_interval: Some(0),
            ..Default::default()
        },
        ..Default::default()
    }
}

// Main function
#[main(window_conf)]
async fn main() -> Result<()> {
    #[cfg(not(target_arch = "wasm32"))]
    env_logger::builder()
        .filter(None, LevelFilter::Info)
        .try_init()
        .wrap_err("Failed to initialize logger")?;
    input::prevent_quit();
    info!("Engine initialized");

    let mut state = State::default();
    info!("State initialized");

    while !(input::is_quit_requested()
        || (input::is_key_pressed(EXIT_KEY) && !cfg!(target_arch = "wasm32")))
    {
        state.update();
        window::clear_background(BACKGROUND);
        state.render().wrap_err("Failed to render state")?;
        window::next_frame().await;
    }
    info!("Loop exited");
    Ok(())
}
