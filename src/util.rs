// Namespaces
pub(crate) use crate::{drone::*, pid::*, state::*};
pub(crate) use eyre::*;
#[cfg(not(target_arch = "wasm32"))]
pub(crate) use log::*;
#[cfg(target_arch = "wasm32")]
pub(crate) use macroquad::logging::*;
pub(crate) use macroquad::{self as mq, math::f32::*, miniquad as minq, *};
pub(crate) use std::{ops::*, *};

// Constants
pub(crate) const TITLE: &str = "Drones";
pub(crate) const FULLSCREEN: bool = if cfg!(target_arch = "wasm32") {
    false
} else {
    true
};
pub(crate) const BACKGROUND: color::Color = color::Color::new(
    0x1D as f32 / 255.0,
    0x20 as f32 / 255.0,
    0x21 as f32 / 255.0,
    1.0,
);
pub(crate) const FOREGROUND: color::Color = color::Color::new(
    0xFB as f32 / 255.0,
    0xF1 as f32 / 255.0,
    0xC7 as f32 / 255.0,
    1.0,
);
pub(crate) const EXIT_KEY: input::KeyCode = input::KeyCode::Escape;
pub(crate) const FULLSCREEN_KEY: input::KeyCode = input::KeyCode::F;
pub(crate) const RESET_DRONE_KEY: input::KeyCode = input::KeyCode::R;
pub(crate) const CENTER_RECT: shapes::DrawRectangleParams = shapes::DrawRectangleParams {
    offset: Vec2::splat(0.5),
    rotation: 0.0,
    color: FOREGROUND,
};
pub(crate) const RANDOMIZE_TARGET_KEY: input::KeyCode = input::KeyCode::Space;
pub(crate) const TRANSPARENT: color::Color = color::Color::new(0.0, 0.0, 0.0, 0.0);

// Fps struct
#[derive(Default)]
pub(crate) struct Fps {
    pub(crate) delta: f32,
    pub(crate) fps: usize,
    avg_delta: f32,
    fps_timer: f32,
}

// Fps implementation
impl Fps {
    // Tick the fps
    pub(crate) fn tick(self: &mut Self) -> () {
        self.delta = mq::time::get_frame_time();
        self.avg_delta = 0.95 * self.avg_delta + 0.05 * self.delta;
        if self.fps_timer > 0.5 {
            self.fps = (1.0 / self.avg_delta) as usize;
            self.fps_timer -= 0.5;
        }
        self.fps_timer += self.delta;
    }
}

// 2D mesh struct
pub(crate) struct Mesh2D {
    pub(crate) origin: Vec2,
    pub(crate) verts: Vec<Vec2>,
}

// 2D mesh implementation
impl Mesh2D {
    // Render mesh
    pub(crate) fn render(
        self: &Self,
        pos: Vec2,
        rot_scale: Vec2,
        color: color::Color,
    ) -> Result<()> {
        if self.verts.is_empty() {
            Err(eyre!("Mesh2D has no vertices"))
        } else {
            let f = |a: Vec2| pos + a.rotate(rot_scale);
            let origin = f(self.origin);
            for verts in self
                .verts
                .iter()
                .copied()
                .chain(iter::once(self.verts[0]))
                .map(f)
                .collect::<Vec<_>>()
                .windows(2)
            {
                shapes::draw_triangle(origin, verts[0], verts[1], color);
            }
            Ok(())
        }
    }
}
