// Namespaces
use crate::util::*;

// Drone struct
pub(crate) struct Drone {
    pub(crate) pos: Vec2,
    vel: Vec2,
    accel: Vec2,
    ang: f32,
    dir: Vec2,
    ang_vel: f32,
    thrusters: (f32, f32),
    thruster_rot_scale: (Vec2, Vec2),
    thruster_rel_pos: (Vec2, Vec2),
    pub(crate) pos_pids: (PID, PID),
    pub(crate) ang_pid: PID,
    target: Vec2,
}

// Drone implementation
impl Drone {
    // Constants
    const DIMS: Vec2 = Vec2::new(0.1, 0.06);
    const BEVEL: f32 = 0.015;
    const MASS: f32 = Self::DIMS.x * Self::DIMS.y - 2.0 * Self::BEVEL * Self::BEVEL;
    const MOI: f32 =
        Self::MASS * (Self::DIMS.x * Self::DIMS.x + Self::DIMS.y * Self::DIMS.y) / 12.0;
    const THRUSTER_POS: (Vec2, Vec2) = (
        Vec2::new(0.5 * Self::DIMS.x, 0.5 * Self::DIMS.y),
        Vec2::new(-0.5 * Self::DIMS.x, 0.5 * Self::DIMS.y),
    );
    const THRUSTER_ANGLE: (f32, f32) = (f32::consts::FRAC_PI_4, 3.0 * f32::consts::FRAC_PI_4);
    const THRUSTER_COLOR: color::Color = color::Color::new(
        0xD7 as f32 / 255.0,
        0x99 as f32 / 255.0,
        0x21 as f32 / 255.0,
        1.0,
    );
    const THRUSTER_FORCE: f32 = -Self::MASS;
    const TARGET_COLOR: color::Color = color::Color::new(
        0xCC as f32 / 255.0,
        0x24 as f32 / 255.0,
        0x1D as f32 / 255.0,
        1.0,
    );
    const TRACE_COLOR: color::Color = color::Color::new(
        0x66 as f32 / 255.0,
        0x5C as f32 / 255.0,
        0x54 as f32 / 255.0,
        0.25,
    );

    // Update the drone
    pub(crate) fn update(self: &mut Self, state: &State) -> () {
        self.accel = Vec2::ZERO;
        self.apply_flight_controller(state);
        self.apply_force_at(
            state,
            Self::THRUSTER_FORCE * self.thruster_rot_scale.0,
            self.thruster_rel_pos.0,
        );
        self.apply_force_at(
            state,
            Self::THRUSTER_FORCE * self.thruster_rot_scale.1,
            self.thruster_rel_pos.1,
        );
        self.apply_accel(state, state.gravity, 0.0);
        self.apply_vel(state);
    }

    // Render the drone
    pub(crate) fn render(self: &Self, state: &State) -> Result<()> {
        shapes::draw_rectangle_ex(
            self.target.x,
            self.target.y,
            0.025,
            0.025,
            shapes::DrawRectangleParams {
                color: Self::TARGET_COLOR,
                ..CENTER_RECT
            },
        );
        let dir = -self.vel.normalize_or_zero();
        let speed = 0.1 * (2.5 * self.vel.length() + 1.0).log2();
        shapes::draw_line(
            self.pos.x,
            self.pos.y,
            self.pos.x + speed * dir.x,
            self.pos.y + speed * dir.y,
            0.05,
            Self::TRACE_COLOR,
        );
        state
            .drone_assets
            .body
            .render(self.pos, self.dir, FOREGROUND)
            .wrap_err("Failed to render body")?;
        state
            .drone_assets
            .thruster
            .render(
                self.pos + self.thruster_rel_pos.0,
                self.thruster_rot_scale.0,
                Self::THRUSTER_COLOR,
            )
            .wrap_err("Failed to render thruster 0")?;
        state
            .drone_assets
            .thruster
            .render(
                self.pos + self.thruster_rel_pos.1,
                self.thruster_rot_scale.1,
                Self::THRUSTER_COLOR,
            )
            .wrap_err("Failed to render thruster 1")?;
        Ok(())
    }

    // Apply velocity
    fn apply_vel(self: &mut Self, state: &State) -> () {
        let delta = state.fps.delta;
        self.pos += delta * self.vel;
        self.ang += delta * self.ang_vel;
        self.dir = Vec2::from_angle(self.ang);
        self.thruster_rot_scale.0 =
            self.thrusters.0 * state.drone_assets.thruster_dir.0.rotate(self.dir);
        self.thruster_rot_scale.1 =
            self.thrusters.1 * state.drone_assets.thruster_dir.1.rotate(self.dir);
        self.thruster_rel_pos.0 = Drone::THRUSTER_POS.0.rotate(self.dir);
        self.thruster_rel_pos.1 = Drone::THRUSTER_POS.1.rotate(self.dir);
    }

    // Apply acceleration
    fn apply_accel(self: &mut Self, state: &State, accel: Vec2, ang_accel: f32) -> () {
        let delta = state.fps.delta;
        self.vel += delta * accel;
        self.accel += accel;
        self.ang_vel += delta * ang_accel;
    }

    // Apply force
    fn apply_force(self: &mut Self, state: &State, force: Vec2, torque: f32) -> () {
        self.apply_accel(state, force / Self::MASS, torque / Self::MOI);
    }

    // Apply force at point
    fn apply_force_at(self: &mut Self, state: &State, force: Vec2, point: Vec2) -> () {
        self.apply_force(state, force, point.perp_dot(force));
    }

    // Apply flight controller
    fn apply_flight_controller(self: &mut Self, state: &State) -> () {
        let delta = state.fps.delta;
        let pos_error = (self.target - self.pos).clamp_length_max(1.0);
        let pos_outs = (
            self.pos_pids.0.update(pos_error.x, delta),
            self.pos_pids.1.update(pos_error.y, delta),
        );
        let ang_error =
            -((self.ang + f32::consts::PI).rem_euclid(f32::consts::TAU) - f32::consts::PI);
        let ang_out = self.ang_pid.update(ang_error, delta);
        self.thrusters.0 = (-pos_outs.0 - pos_outs.1 - ang_out).clamp(0.0, 1.0);
        self.thrusters.1 = (pos_outs.0 - pos_outs.1 + ang_out).clamp(0.0, 1.0);
    }

    // Reset drone
    pub(crate) fn reset(self: &mut Self) -> () {
        let drone = mem::take(self);
        *self = Self {
            target: drone.target,
            pos_pids: drone.pos_pids,
            ang_pid: drone.ang_pid,
            ..*self
        };
    }

    // Generate target
    pub(crate) fn gen_target(self: &mut Self) -> () {
        let f = || rand::gen_range(-1.0, 1.0);
        self.target = Vec2::new(f(), f());
    }
}

// Default drone implementation
impl Default for Drone {
    // Default drone
    fn default() -> Self {
        let mut drone = Self {
            pos: Vec2::ZERO,
            vel: Vec2::ZERO,
            accel: Vec2::ZERO,
            ang: 0.0,
            dir: Vec2::X,
            ang_vel: 0.0,
            thrusters: (0.0, 0.0),
            thruster_rot_scale: (Vec2::ZERO, Vec2::ZERO),
            thruster_rel_pos: (Vec2::ZERO, Vec2::ZERO),
            pos_pids: (PID::default(), PID::default()),
            ang_pid: PID::default(),
            target: Vec2::ZERO,
        };
        drone.gen_target();
        drone
    }
}

// Drone assets struct
pub(crate) struct DroneAssets {
    body: Mesh2D,
    thruster: Mesh2D,
    thruster_dir: (Vec2, Vec2),
}

// Default drone assets implementation
impl Default for DroneAssets {
    // Create new drone assets
    fn default() -> Self {
        Self {
            body: Mesh2D {
                origin: Vec2::ZERO,
                verts: vec![
                    (Vec2::X + Vec2::Y) * (0.5 * Drone::DIMS - Vec2::X * Drone::BEVEL),
                    (Vec2::X + Vec2::Y) * (0.5 * Drone::DIMS - Vec2::Y * Drone::BEVEL),
                    (Vec2::X - Vec2::Y) * (0.5 * Drone::DIMS - Vec2::Y * Drone::BEVEL),
                    (Vec2::X - Vec2::Y) * (0.5 * Drone::DIMS - Vec2::X * Drone::BEVEL),
                    (-Vec2::X - Vec2::Y) * (0.5 * Drone::DIMS - Vec2::X * Drone::BEVEL),
                    (-Vec2::X - Vec2::Y) * (0.5 * Drone::DIMS - Vec2::Y * Drone::BEVEL),
                    (-Vec2::X + Vec2::Y) * (0.5 * Drone::DIMS - Vec2::Y * Drone::BEVEL),
                    (-Vec2::X + Vec2::Y) * (0.5 * Drone::DIMS - Vec2::X * Drone::BEVEL),
                ],
            },
            thruster: Mesh2D {
                origin: Vec2::ZERO,
                verts: vec![
                    Vec2::X * 1.5 * Drone::BEVEL,
                    Vec2::Y * 0.75 * Drone::BEVEL,
                    -Vec2::Y * 0.75 * Drone::BEVEL,
                ],
            },
            thruster_dir: (
                Vec2::from_angle(Drone::THRUSTER_ANGLE.0),
                Vec2::from_angle(Drone::THRUSTER_ANGLE.1),
            ),
        }
    }
}
